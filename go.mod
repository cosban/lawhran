module gitlab.com/cosban/lawhran

go 1.16

require (
	github.com/PuerkitoBio/goquery v1.7.0
	github.com/robertkrimen/otto v0.0.0-20211024170158-b87d35c0b86f
	golang.org/x/net v0.0.0-20211203184738-4852103109b8
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11
)
