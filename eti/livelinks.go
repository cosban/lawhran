package eti

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/robertkrimen/otto/ast"
	"github.com/robertkrimen/otto/parser"
)

func (e *ETIClient) SubscribeToTopic(topicID uint) (chan []Message, error) {
	doc, err := e.getQueryableDocument(fmt.Sprintf("https://boards.endoftheinter.net/showmessages.php?topic=%d", topicID))
	if err != nil {
		return nil, err
	}
	src := doc.Find(".infobar#u0_4 + script").First().Text()
	return e.subscribe(src, "boards.", fmt.Sprintf("topic=%d", topicID))
}

// SubscribeToThread is used for private messages
func (e *ETIClient) SubscribeToThread(threadID uint) (chan []Message, error) {
	doc, err := e.getQueryableDocument(fmt.Sprintf("https://endoftheinter.net/inboxthread.php?thread=%d", threadID))
	if err != nil {
		return nil, err
	}
	src := doc.Find(".infobar#u0_3 + script").First().Text()
	return e.subscribe(src, "", fmt.Sprintf("pm=%d", threadID))
}

func (e *ETIClient) subscribe(src string, subdomain, idParam string) (chan []Message, error) {
	subscriber := make(chan []Message)
	subscriptionID, lastPost, err := getSubscriptionInfo(src)
	if err != nil {
		return nil, err
	}
	listen := func() {
		for {
			resp, err := e.client.Post(
				"https://evt0.endoftheinter.net/subscribe",
				"application/json",
				bytes.NewBufferString(fmt.Sprintf(`{"%s":0"}`, subscriptionID)),
			)
			if err != nil {
				log.Print(err)
				continue
			}
			go func() {
				defer resp.Body.Close()
				b, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Print(err)
				}
				var data map[string]int
				if err := json.Unmarshal(b[1:], &data); err != nil {
					log.Print(err)
				}
				if value, ok := data[subscriptionID]; ok {

					tempLastPost := lastPost
					lastPost = value
					content, err := e.callMoreMessages(subdomain, idParam, tempLastPost, value)
					if err != nil {
						log.Print(err)
					}
					doc, err := goquery.NewDocumentFromReader(strings.NewReader(content))
					if err != nil {
						log.Print(err)
					}
					subscriber <- e.extractMessages(doc)
				}
			}()
		}
	}
	go listen()
	return subscriber, nil
}

func getSubscriptionInfo(src string) (string, int, error) {
	program, err := parser.ParseFile(nil, "", src, 0)
	if err != nil {
		return "", 0, err
	}
	// yikes
	subscriptionArr := program.Body[0].(*ast.ExpressionStatement).
		Expression.(*ast.CallExpression).
		ArgumentList[0].(*ast.FunctionLiteral).
		Body.(*ast.BlockStatement).
		List[0].(*ast.ExpressionStatement).
		Expression.(*ast.NewExpression).
		ArgumentList[6].(*ast.ArrayLiteral)
	return subscriptionArr.Value[0].(*ast.StringLiteral).Value, int(subscriptionArr.Value[1].(*ast.NumberLiteral).Value.(int64)), nil
}

func (e *ETIClient) callMoreMessages(subdomain, idParam string, lastPost, value int) (string, error) {
	resp, err := e.client.Get(
		fmt.Sprintf("https://%sendoftheinter.net/moremessages.php?%s&old=%d&new=%d", subdomain, idParam, lastPost, value),
	)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	var content string
	err = json.Unmarshal(bytes.TrimPrefix(body, []byte{'}'}), &content)
	return content, err
}
