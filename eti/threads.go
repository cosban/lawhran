package eti

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"golang.org/x/time/rate"

	"github.com/PuerkitoBio/goquery"
)

func (e *ETIClient) GetPrivateMessageCount() (string, error) {
	doc, err := e.getQueryableDocument("https://endoftheinter.net/inbox.php")
	if err != nil {
		return "", err
	}
	return doc.Find("#userbar_pms_count").First().Text(), nil
}

func (e *ETIClient) GetUnreadPrivateMessages() ([]ETITopic, error) {
	doc, err := e.getQueryableDocument("https://endoftheinter.net/inbox.php")
	if err != nil {
		return nil, err
	}
	var topics []ETITopic
	doc.Find("tr").Each(func(i int, selection *goquery.Selection) {
		if selection.Find("td>b").Size() == 0 {
			return
		}
		message := ETITopic{}
		selection.Find("td").Each(func(i int, selection *goquery.Selection) {
			switch i {
			case 0:
				if anchor, ok := selection.Find("a").First().Attr("href"); ok {
					message.Title = selection.Text()
					id, _ := strconv.ParseInt(strings.TrimPrefix(anchor, "/inboxthread.php?thread="), 10, 64)
					message.TopicID = id
				}
			case 1:
				message.Author = selection.Text()
			}
		})
		topics = append(topics, message)
	})
	return topics, nil
}

func (e *ETIClient) ExtractThreadMeta(threadID uint) (*ETITopic, error) {
	doc, err := e.getQueryableDocument(fmt.Sprintf("https://endoftheinter.net/inboxthread.php?thread=%d", threadID))
	if err != nil {
		return nil, err
	}
	return e.extractMeta(doc)
}

func (e *ETIClient) ReadPrivateMessage(threadID uint) (*ETITopic, error) {
	meta, err := e.ExtractThreadMeta(threadID)
	if err != nil {
		return nil, err
	}
	limiter := rate.NewLimiter(rate.Every(1*time.Second), 1)
	for i := 0; i < meta.Pages; i++ {
		limiter.Wait(context.Background())
		location := fmt.Sprintf("https://endoftheinter.net/inboxthread.php?thread=%d&page=%d", threadID, i+1)
		doc, err := e.getQueryableDocument(location)
		if err != nil {
			return nil, err
		}
		doc.Find("div.message-container").Each(func(i int, selection *goquery.Selection) {
			message, err := e.readPost(selection)
			if err != nil {
				return
			}
			meta.Messages = append(meta.Messages, *message)
		})
	}
	return meta, nil
}

func (e *ETIClient) NewPrivateMessage(userid uint, title, content string) (uint, error) {
	doc, err := e.getQueryableDocument(fmt.Sprintf("https://endoftheinter.net/postmsg.php?puser=%d", userid))
	if err != nil {
		return 0, err
	}
	var h string
	attr := doc.Find("form input[name='h']").Get(0).Attr
	for _, at := range attr {
		if at.Key == "value" {
			h = at.Val
			break
		}
	}
	params := url.Values{}

	params.Add("title", title)
	params.Add("puser", fmt.Sprintf("%d", userid))
	params.Add("message", fmt.Sprintf("<pre>%s</pre>\n\n---\ni am literally a robot.", content))
	params.Add("h", h)
	params.Add("submit", "Send Message")
	return e.postMessage(params)
}

func (e *ETIClient) PostInThread(threadID uint, content string) error {
	thread, err := e.ExtractThreadMeta(threadID)
	if err != nil {
		return err
	}
	params := url.Values{}
	params.Add("pm", strconv.Itoa(int(threadID)))
	params.Add("message", fmt.Sprintf("<pre>%s</pre>\n\n---\ni am literally a robot.", content))
	params.Add("h", thread.H)
	params.Add("-ajaxCounter", "1")
	if e.Dry {
		return nil
	}
	req, _ := http.NewRequest("POST", "https://boards.endoftheinter.net/async-post.php", bytes.NewBuffer([]byte(params.Encode())))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := e.client.Do(req)
	if err != nil {
		log.Println(resp)
		return err
	}
	defer resp.Body.Close()
	response := map[string]string{}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(body[1:], &response)
	if err != nil {
		return err
	}
	if response["success"] == "Message posted." {
		log.Println(response["success"])
	}
	return nil
}
