package eti

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type Message struct {
	Poster   string
	PosterID uint64
	Time     time.Time
	Message  string
}

func (e *ETIClient) ExtractTopicMeta(topicID uint) (*ETITopic, error) {
	doc, err := e.getQueryableDocument(fmt.Sprintf("https://boards.endoftheinter.net/showmessages.php?topic=%d", topicID))
	if err != nil {
		return nil, err
	}
	return e.extractMeta(doc)
}

type ETITopic struct {
	Title       string
	Pages       int
	TopicID     int64
	Author      string
	AuthorID    int64
	Locked      bool
	Tags        []string
	H           string
	NumMessages string
	Messages    []Message
}

func (e *ETIClient) ListAllReadableTopics(tags string, earliest time.Time) ([]ETITopic, error) {
	topics, next, err := e.listTopicsOnPage(fmt.Sprintf("https://boards.endoftheinter.net/topics/%s", tags))
	if err != nil {
		return nil, err
	}
	for next != nil {
		if valid, err := tsIsValid(earliest, *next); err != nil {
			return nil, err
		} else if !valid {
			break
		}
		var merge []ETITopic
		merge, next, err = e.listTopicsOnPage("https://boards.endoftheinter.net" + *next)
		topics = append(topics, merge...)
	}
	return topics, nil
}

func tsIsValid(earliest time.Time, href string) (bool, error) {
	page, err := url.Parse(href)
	if err != nil {
		return false, err
	}
	tsAttr := page.Query().Get("ts")
	ts, err := strconv.ParseInt(tsAttr, 10, 64)
	if err != nil {
		return false, err
	}
	return time.Unix(ts, 0).After(earliest), nil
}

func (e *ETIClient) listTopicsOnPage(url string) ([]ETITopic, *string, error) {
	doc, err := e.getQueryableDocument(url)
	if err != nil {
		return nil, nil, err
	}
	var topics []ETITopic
	doc.Find("tr").Each(func(i int, selection *goquery.Selection) {
		if i == 0 {
			return
		}
		anchor := selection.Find(".fl a")
		href, ok := anchor.Attr("href")
		var topicID int64
		if ok {
			topicID, _ = strconv.ParseInt(strings.TrimPrefix(href, "//boards.endoftheinter.net/showmessages.php?topic="), 10, 64)
		}

		topic := ETITopic{
			Title:       anchor.Text(),
			Author:      selection.Find("td:nth-child(2)").Text(),
			TopicID:     topicID,
			NumMessages: selection.Find("td:nth-child(3)").Text(),
		}
		topics = append(topics, topic)
	})
	if next, ok := doc.Find("a[href*='?ts=']").Attr("href"); ok {
		return topics, &next, nil
	}
	return topics, nil, nil
}

func (e *ETIClient) ListTopics(tags string) ([]ETITopic, error) {
	topics, _, err := e.listTopicsOnPage(fmt.Sprintf("https://boards.endoftheinter.net/topics/%s", tags))
	return topics, err
}

func (e *ETIClient) CreateNewTopic(title, content string) (uint, error) {
	doc, err := e.getQueryableDocument("https://boards.endoftheinter.net/postmsg.php?tag=LUE")
	if err != nil {
		return 0, err
	}
	params := url.Values{}

	params.Add("title", title)
	params.Add("tag", "LUE")
	params.Add("message", fmt.Sprintf("<pre>%s</pre>\n\n---\ni am literally a robot.", content))
	params.Add("h", e.mustGetHParam(doc))
	params.Add("submit", "Post Message")
	return e.postMessage(params)
}

func (e *ETIClient) PostInTopic(topicID uint, content string) error {
	topic, err := e.ExtractTopicMeta(topicID)
	if err != nil {
		return err
	}
	params := url.Values{}
	params.Add("topic", strconv.Itoa(int(topicID)))
	params.Add("message", fmt.Sprintf("<pre>%s</pre>\n\n---\ni am literally a robot.", content))
	params.Add("h", topic.H)
	params.Add("-ajaxCounter", "1")
	if e.Dry {
		return nil
	}
	req, _ := http.NewRequest("POST", "https://boards.endoftheinter.net/async-post.php", bytes.NewBuffer([]byte(params.Encode())))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := e.client.Do(req)
	if err != nil {
		log.Println(resp)
		return err
	}
	defer resp.Body.Close()
	response := map[string]string{}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(body[1:], &response)
	if err != nil {
		return err
	}
	if response["success"] == "Message posted." {
		log.Println(response["success"])
	}
	return nil
}

func (e *ETIClient) ReadTopicUserMessages(topicID, userID uint) (*ETITopic, error) {
	doc, err := e.getQueryableDocument(fmt.Sprintf("https://boards.endoftheinter.net/showmessages.php?topic=%d&u=%d", topicID, userID))
	if err != nil {
		return nil, err
	}
	meta, err := e.extractMeta(doc)
	if err != nil {
		return nil, err
	}

	for i := 0; i < meta.Pages; i++ {
		location := fmt.Sprintf("https://boards.endoftheinter.net/showmessages.php?topic=%d&u=%d&page=%d", topicID, userID, i+1)
		doc, err := e.getQueryableDocument(location)
		if err != nil {
			return nil, err
		}
		doc.Find("div.message-container").Each(func(i int, selection *goquery.Selection) {
			message, err := e.readPost(selection)
			if err != nil {
				return
			}
			meta.Messages = append(meta.Messages, *message)
		})
	}
	return meta, nil
}

func (e *ETIClient) ReadTopicMessages(topicID uint) (*ETITopic, error) {
	meta, err := e.ExtractTopicMeta(topicID)
	if err != nil {
		return nil, err
	}
	for i := 0; i < meta.Pages; i++ {
		location := fmt.Sprintf("https://boards.endoftheinter.net/showmessages.php?topic=%d&page=%d", topicID, i+1)
		doc, err := e.getQueryableDocument(location)
		if err != nil {
			return nil, err
		}
		doc.Find("div.message-container").Each(func(i int, selection *goquery.Selection) {
			message, err := e.readPost(selection)
			if err != nil {
				return
			}
			meta.Messages = append(meta.Messages, *message)
		})
	}
	return meta, nil
}
