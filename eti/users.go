package eti

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

type UserStatus struct {
	ID         uint      `json:"id"`
	Username   string    `json:"username"`
	LastActive time.Time `json:"last_active"`
}

type UserMeta struct {
	ID         uint
	Username   string
	AdminTags  []string `json:"-"`
	ModTags    []string `json:"-"`
	GoodTokens int64
	BadTokens  int64
	Banned     bool
	LastActive time.Time
	Joined     time.Time
	LastPost   time.Time
}

func (e *ETIClient) ScrapePostHistory(i uint) (*UserMeta, error) {
	user, err := e.ScrapeUser(i)
	if err != nil {
		return nil, err
	}

	location := "https://boards.endoftheinter.net/messages.php?userid=%d"
	req, err := http.NewRequest("GET", fmt.Sprintf(location, i), nil)
	if err != nil {
		return nil, err
	}
	resp, err := e.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf(resp.Status)
	}
	doc, _ := goquery.NewDocumentFromReader(resp.Body)
	rows := doc.Find("div.message-top")
	if rows.Length() > 0 {
		row := rows.First()
		if row != nil {
			lastPostStr := row.Text()
			lastPostStr = lastPostStr[strings.Index(lastPostStr, "| Posted:")+len("| Posted :"):]
			lastPostStr = lastPostStr[:strings.Index(lastPostStr, "|")]

			user.LastPost, err = time.Parse("1/2/2006 03:04:05 PM", strings.TrimSpace(lastPostStr))
			if err != nil {
				return nil, errors.New("the time formatting didn't work for " + lastPostStr)
			}
		}
	}
	return user, nil
}

func (e *ETIClient) ScrapeUser(i uint) (*UserMeta, error) {
	location := "https://endoftheinter.net/profile.php?user=%d"
	req, err := http.NewRequest("GET", fmt.Sprintf(location, i), nil)
	if err != nil {
		return nil, err
	}
	resp, err := e.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, errors.New(fmt.Sprintf("%d", resp.StatusCode))
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}
	user := UserMeta{ID: i}
	doc.Find("td:first-child").Each(func(i int, sel *goquery.Selection) {
		data := sel.Next()
		if data.Text() == "" {
			return
		}
		switch sel.Text() {
		case "User Name":
			user.Username = data.Text()
		case "Status":
			user.Banned = data.Text() == "Banned"
		case "Administrator of":
			user.AdminTags = strings.Split(data.Text(), " • ")
		case "Moderator of":
			user.ModTags = strings.Split(data.Text(), " • ")
		case "Good\u00a0Tokens":
			user.GoodTokens, _ = strconv.ParseInt(data.Text(), 10, 64)
			if err != nil {
				panic(err)
			}
		case "Bad Tokens":
			user.BadTokens, err = strconv.ParseInt(data.Text(), 10, 64)
			if err != nil {
				panic(err)
			}
		case "Account Created":
			user.Joined, err = time.Parse("1/2/2006", data.Text())
			if err != nil {
				panic(err)
			}
		case "Last Active":
			active := strings.TrimSuffix(data.Text(), " (online now)")
			user.LastActive, err = time.Parse("1/2/2006", active)
			if err != nil {
				panic(err)
			}
		}
	})
	return &user, nil
}
