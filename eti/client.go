package eti

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/http2"

	"github.com/PuerkitoBio/goquery"
	"golang.org/x/time/rate"
)

var etiClient *ETIClient

type ETIClient struct {
	Dry          bool
	client       http.Client
	postLimiter  *rate.Limiter
	queryLimiter *rate.Limiter
}

func DryClient(username, password string) *ETIClient {
	c := Client(username, password)
	c.Dry = true
	return c
}

func NonSingletonClient(username, password string) *ETIClient {
	return newClient(username, password)
}

func Client(username, password string) *ETIClient {
	if etiClient == nil {
		etiClient = newClient(username, password)
	}
	return etiClient
}

func newClient(username, password string) *ETIClient {
	cookiejar, _ := cookiejar.New(nil)
	client := http.Client{
		Transport: &http2.Transport{},
		Jar:       cookiejar,

		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	urlValues := url.Values{}
	urlValues.Add("b", username)
	urlValues.Add("p", password)
	r, _ := http.NewRequest("POST", "https://endoftheinter.net", strings.NewReader(urlValues.Encode()))
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Accept", "*/*")

	resp, err := client.Do(r)
	if err != nil {
		log.Printf("Error logging into ETI: %s", err.Error())
		return nil
	}
	if resp.StatusCode != 200 && resp.StatusCode != 302 {
		log.Printf("Error logging into ETI: code: %d, status: %s", resp.StatusCode, resp.Status)
		return nil
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading response body: %s", err.Error())
		return nil
	}
	if strings.Contains(string(body), "<input class=\"text\" name=\"b\" />") {
		log.Println("invalid credentials")
		return nil
	}
	defer resp.Body.Close()
	return &ETIClient{
		client:       client,
		postLimiter:  rate.NewLimiter(rate.Every(31*time.Second), 1),
		queryLimiter: rate.NewLimiter(rate.Every(100*time.Millisecond), 1),
	}
}

func (e *ETIClient) getQueryableDocument(url string) (*goquery.Document, error) {
	if err := e.queryLimiter.Wait(context.Background()); err != nil {
		return nil, err
	}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	resp, err := e.client.Do(req)

	if resp.StatusCode != 200 && resp.StatusCode != 302 {
		return e.getQueryableDocument(url)
	}
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	writer, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return goquery.NewDocumentFromReader(bytes.NewBuffer(writer))
}

func (e *ETIClient) postMessage(params url.Values) (uint, error) {
	if e.Dry {
		return 0, nil
	}
	req, _ := http.NewRequest("POST", "https://boards.endoftheinter.net/postmsg.php", bytes.NewBuffer([]byte(params.Encode())))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	if err := e.postLimiter.Wait(context.TODO()); err != nil {
		return 0, err
	}
	resp, err := e.client.Do(req)
	if err != nil {
		log.Println(resp)
		return 0, err
	}
	if resp.StatusCode != 302 {
		log.Println(resp)
		return 0, errors.New("was not redirected: " + resp.Status)
	}
	redirect, err := resp.Location()
	if err != nil {
		panic(err)
	}
	threadNumber := redirect.String()[strings.Index(redirect.String(), "=")+1:]
	num, err := strconv.ParseUint(threadNumber, 10, 64)
	return uint(num), err
}

func (e *ETIClient) extractMeta(doc *goquery.Document) (*ETITopic, error) {
	title := doc.Find("h1").Text()
	tags := []string{}
	doc.Find("h2 > div> a").Each(func(i int, s *goquery.Selection) {
		tags = append(tags, s.Text())
	})
	pagesStr := doc.Find("div.infobar span").First().Text()
	pages, _ := strconv.Atoi(pagesStr) // set to zero if can't parse
	h, ok := e.getHParam(doc)
	return &ETITopic{
		Title:  title,
		Pages:  pages,
		Tags:   tags,
		H:      h,
		Locked: !ok,
	}, nil
}

func (e *ETIClient) mustGetHParam(doc *goquery.Document) string {
	var h string
	input := doc.Find("form input[name='h']")
	if input.Length() == 0 {
		panic("no H")
	}
	attr := input.Get(0).Attr
	for _, at := range attr {
		if at.Key == "value" {
			h = at.Val
			break
		}
	}
	return h
}

func (e *ETIClient) getHParam(doc *goquery.Document) (string, bool) {
	var h string
	input := doc.Find("form input[name='h']")
	if input.Length() == 0 {
		return "", false
	}
	attr := input.Get(0).Attr
	for _, at := range attr {
		if at.Key == "value" {
			h = at.Val
			break
		}
	}
	return h, true
}
