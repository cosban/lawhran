package eti

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

type Drama struct {
	Parse struct {
		Text struct {
			Content string `json:"*"`
		}
		Externallinks []string
	}
}

func (e *ETIClient) RequestDrama() (*Drama, error) {
	req, err := http.NewRequest("GET", "https://wiki.endoftheinter.net/api.php?action=parse&page=Dramalinks/current&format=json", nil)
	if err != nil {
		return nil, err
	}
	resp, err := e.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	log.Println(string(content))
	drama := new(Drama)
	return drama, json.Unmarshal(content, drama)
}
