package eti

import (
	"strconv"
	"strings"
)

type Statistics struct {
	Messages int64 `json:"messages"`
	Users    int64 `json:"users"`
}

func (e *ETIClient) ScrapeStats() (uint, error) {
	doc, err := e.getQueryableDocument("https://endoftheinter.net/stats.php")
	if err != nil {
		return 0, err
	}
	messagesStr := strings.TrimSpace(doc.Find(`a[href*="graph=3"]`).First().Nodes[0].NextSibling.Data)
	messages, _ := strconv.ParseUint(messagesStr, 10, 64)
	return uint(messages), nil
}

func (e *ETIClient) GetMaxUserID() (uint, error) {
	userlist, err := e.getQueryableDocument("https://endoftheinter.net/userlist.php")
	if err != nil {
		return 0, err
	}
	href, ok := userlist.Find("div.body > div.infobar > a[href]:last-child").Attr("href")
	if !ok {
		return 0, err
	}
	doc, err := e.getQueryableDocument("https://endoftheinter.net" + href)
	if err != nil {
		return 0, err
	}
	last, ok := doc.Find(`tr:last-child a[href*="user"]`).Attr("href")
	if !ok {
		return 0, err
	}
	str := strings.TrimPrefix(last, "//endoftheinter.net/profile.php?user=")
	userid, err := strconv.ParseUint(str, 10, 64)
	return uint(userid), err
}
