package eti

import (
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

func (e *ETIClient) EditPost(topicID, postID uint, content string) (uint, error) {
	location := fmt.Sprintf("https://boards.endoftheinter.net/showmessages.php?topic=%d&id=%d", topicID, postID)
	doc, err := e.getQueryableDocument(location)
	if err != nil {
		return 0, err
	}
	params := url.Values{}

	params.Add("topic", fmt.Sprintf("%d", topicID))
	params.Add("id", fmt.Sprintf("%d", postID))
	params.Add("message", fmt.Sprintf("<pre>%s</pre>\n\n---\ni am literally a robot.", content))
	params.Add("h", e.mustGetHParam(doc))
	params.Add("submit", "Submit Revision")
	return e.postMessage(params)
}

func (e *ETIClient) ReadPost(topicID, postID uint) (*Message, error) {
	location := fmt.Sprintf("https://boards.endoftheinter.net/showmessages.php?topic=%d&id=%d", topicID, postID)
	doc, err := e.getQueryableDocument(location)
	if err != nil {
		return nil, err
	}
	selection := doc.Find("div.message-container").First()
	return e.readPost(selection)
}

func (e *ETIClient) readPost(selection *goquery.Selection) (*Message, error) {
	messageTop := strings.Split(selection.Find("div.message-top").First().Text(), "|")
	var poster string
	var posted time.Time
	posterURL, exists := selection.Find("div.message-top a").First().Attr("href")
	var posterID uint64
	if exists {
		posterURL = posterURL[strings.Index(posterURL, "user=")+len("user="):]
		posterID, _ = strconv.ParseUint(posterURL, 10, 64)
	}
	for _, top := range messageTop {
		top = strings.TrimSpace(top)
		if strings.HasPrefix(top, "From: ") {
			poster = strings.TrimPrefix(top, "From: ")
		} else if strings.HasPrefix(top, "Posted: ") {
			postedStr := strings.TrimPrefix(top, "Posted: ")
			posted, _ = time.ParseInLocation("1/2/2006 15:04:05 PM", postedStr, time.Now().Location())
		}
	}
	content := selection.Find("td.message").First().Children().Remove().End().Text()
	return &Message{Poster: poster, Time: posted, Message: content, PosterID: posterID}, nil
}

func (e *ETIClient) extractMessages(doc *goquery.Document) []Message {
	var messages []Message
	doc.Find("div.message-container").Each(func(i int, selection *goquery.Selection) {
		message, err := e.readPost(selection)
		if err != nil {
			log.Println(err)
			return
		}
		messages = append(messages, *message)
	})
	return messages
}
