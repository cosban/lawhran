package main

import (
	"log"

	"gitlab.com/cosban/lawhran/examples/secret_santa"
)

func main() {
	santa := secret_santa.Load("santa.json")

	regular := santa.Shuffle(santa.Regular)
	expensive := santa.Shuffle(santa.Expensive)

	sendPrivateMessages(santa, "regular", regular)
	sendPrivateMessages(santa, "expensive", expensive)
	log.Println("done shuffling")
}

func sendPrivateMessages(santa *secret_santa.Santa, tier string, users []secret_santa.User) {
	for _, user := range users {
		_, err := santa.SendSantaPM(user, tier)
		if err != nil {
			log.Println(err)
		}
	}
}
