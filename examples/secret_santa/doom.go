package secret_santa

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"gitlab.com/cosban/lawhran/eti"
)

const (
	TOPIC_ID = 10272273
	OP       = 202770551
	TEMPLATE = `Two levels of secret santa this year. Regular and Expensive. You can join one or both of them if you want to.
  
<b>How do I join?</b>
Post in this topic and I'll add you to the list. If you don't specify whether you want to be on the expensive list, you will only be added to the regular list.

<spoiler caption="FAQ"> 
<b>How will I know who my recipient is?</b>
I will PM you their details along with any suggestions/wishlists they have sent me on December 5th.
  

<b>What happens when I join?</b>
I'll send you a PM with the information I'll need from you in order to make this fun. This will include the following:
* Shipping Details
* At least 3 things you are interested in
* Optional: things which you don't want 
* Optional: wishlists (Amazon/Alibaba/William Sonoma)
* Optional: Are you planning on donating your received items?
  

<b>How long do I have to decide if I want to do it?</b>
Signups will close around end of day December 11th. 
  

<b>What happens if I don't get my stuff?</b>
PM me and or post in this topic. They will be suspended until they send something out.
  

<b>What are the spending limits?</b>
For the regular tier. The MINIMUM is $10 and the soft maximum is $50. You can go over it if you really want to, but it is not expected of you.
For the expensive tier, the MINIMUM is $100 with no maximum.
These prices do not include shipping costs.
  

<b>I'm not comfortable giving other yetis my information</b>
Then don't join lol. 
  

<b>What if someone misuses my shipping info?</b>
Report that shit. They'll be banned.
</spoiler>

So who's in for some yeti gift giving?

Regular Tier Yetis:
%s

Expensive Tier Yetis:
%s

ETI GOLD Tier - these users are planning on donating their received goods to children's hospitals or a local charity:`
)

type User struct {
	ID             uint   `json:"id,string,omitempty"`
	Name           string `json:"name,omitempty"`
	Recipient      uint   `json:"recipient,omitempty"`
	Address        string `json:"address,omitempty"`
	Requests       string `json:"requests,omitempty"`
	AnonTopic      uint   `json:"anon_topic,omitempty"`
	RecipientTopic uint   `json:"recipient_topic,omitempty"`
}

func (u User) String() string {
	return fmt.Sprintf("{ID: %d, Name: %s, Recipient: %d}", u.ID, u.Name, u.Recipient)
}

type Santa struct {
	Users     map[uint]User `json:"-"`
	Regular   []User        `json:"regular"`
	Expensive []User        `json:"expensive"`
	tracking  map[uint]bool
	client    *eti.ETIClient
}

func New() *Santa {
	return &Santa{
		client:   eti.Client(),
		tracking: map[uint]bool{},
	}
}

func Load(filename string) *Santa {
	santa := Santa{}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(b, &santa); err != nil {
		panic(err)
	}
	santa.client = eti.Client()
	santa.Users = map[uint]User{}
	for _, user := range append(santa.Regular, santa.Expensive...) {
		santa.Users[user.ID] = user
	}
	return &santa
}

func (s *Santa) Shuffle(users []User) []User {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	var available []User
	for _, user := range users {
		available = append(available, user)
	}
	for senderIndex, user := range users {
		index := -1
		//  generate     don't send to self                don't send to someone sending to self
		for index < 0 || available[index].ID == user.ID || user.ID == available[index].Recipient {
			index = r.Intn(len(available))
		}
		user.Recipient = available[index].ID
		available = append(available[:index], available[index+1:]...)
		users[senderIndex] = user
	}
	return users
}

func (s *Santa) AddUsers(regular, expensive []User) error {
	if err := s.ScrapeOP(); err != nil {
		return err
	}
	if err := s.sendPMsFor(regular, &s.Regular); err != nil {
		return err
	}
	if err := s.sendPMsFor(expensive, &s.Expensive); err != nil {
		return err
	}
	_, err := s.client.EditPost(TOPIC_ID, OP, fmt.Sprintf(TEMPLATE, stringifyUsers(s.Regular), stringifyUsers(s.Expensive)))
	return err
}

func (s *Santa) sendPMsFor(users []User, list *[]User) error {
	for _, u := range users {
		if _, ok := s.tracking[u.ID]; !ok {
			s.tracking[u.ID] = true
			if _, err := s.SendFirstPM(u.ID); err != nil {
				return err
			}
		}
		*list = append(*list, u)
	}
	return nil
}

func stringifyUsers(users []User) string {
	b := strings.Builder{}
	prefix := "* "
	for _, u := range users {
		b.WriteString(prefix)
		b.WriteString(fmt.Sprintf("%s:%d", u.Name, u.ID))
		prefix = "\n* "
	}
	return b.String()
}

func (s *Santa) SendSantaPM(user User, tier string) (uint, error) {
	recipient := s.Users[user.Recipient]
	return s.client.NewPrivateMessage(
		user.ID,
		fmt.Sprintf("Secret Santa Results: %s tier", tier),
		fmt.Sprintf(`Happy Holidays!
You are recieving this private message because you signed up for this year's Secret Santa.
  
You have been assigned the following user for the %s tier: %s 
  
Shipping details: <quote>%s</quote>
  
Requests: <quote>%s</quote>
  
<b>If you have any questions please direct them to cosban and he'll attempt to get you sorted. If cosban IS your person, PM a mod so they can relay the message.</b>`, tier, recipient.Name, recipient.Address, recipient.Requests))
}

func (s *Santa) SendFirstPM(userID uint) (uint, error) {
	return s.client.NewPrivateMessage(userID, "Secret Santa of Doom", `* Shipping Details
* At least 3 things you are interested in
* Optional: things which you don't want
* Optional: wishlists (Amazon/Alibaba/William Sonoma)
* Optional: Are you planning on donating your received items?

Note: if you are in both tiers, you will have two recipients but also receive from two people`)
}

func (s *Santa) ScrapeOP() error {
	message, err := s.client.ReadPost(TOPIC_ID, OP)
	if err != nil {
		return err
	}
	expensive := false
	begin := strings.Index(message.Message, "Regular Tier Yetis:") + len("Regular Tier Yetis:")
	lines := strings.Split(message.Message[begin:], "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "Expensive Tier Yetis:") {
			expensive = true
		}
		if strings.HasPrefix(line, "* ") {
			user_and_id := strings.Split(line[2:], ":")
			id, err := strconv.Atoi(user_and_id[1])
			if err != nil {
				log.Println(user_and_id)
				return err
			}
			user := User{Name: user_and_id[0], ID: uint(id)}
			s.tracking[user.ID] = true
			if expensive {
				s.Expensive = append(s.Expensive, user)
			} else {
				s.Regular = append(s.Regular, user)
			}
		}
	}
	return nil
}
