package main

import (
	"log"
	"strings"
	"sync"
	"time"

	"gitlab.com/cosban/lawhran/examples/roulette"

	"gitlab.com/cosban/lawhran/eti"
)

var OP = `Are you feeling lucky?

Let's see who the luckiest yeti is and measure it by giving out points. Here's how they'll be given out:
  
* 1 point:   Each shot you survive
* 5 points:  For a spin where shooting would have killed you
* -5 points: For a spin where shooting would NOT have killed you.
  
If you get shot, your points go back to 0.
  
<b>Let's make this more interesting by giving out a prize. Caldor.</b>
Highest score by either the end of the topic, or by 2021-12-11 when I get shut off (which ever comes first) will win a $50 Darden gift card. 
<spoiler caption="restaurants">Olive Garden, Longhorn Steakhouse, Cheddar's, Yard House, The Capital Grill, Seasons 52, Bahama Breeze, Or Eddie V's</spoiler>
  
note: there are probably bugs in the bot. I'll try to be as fair as possible, but don't bitch at cosban, he's just trying to do cool shit and have fun. 

<b>How to play:</b>
The commands to performed just like hurt/heal in that they must be on their own line. You can go as many times as you want in a row, but only one action per post.
* spin
OR
* shoot

My revolver has six chambers and one bullet.

Scoreboard:`

func main() {
	client := eti.Client("", "")

	config := roulette.LoadGame("roulette.json")
	if config.Topic == 0 {
		id, err := client.CreateNewTopic("Let's play some Russian Roulette", OP)
		if err != nil {
			panic(err)
		}
		config.Topic = id
		if err := roulette.SaveGame("roulette.json", config); err != nil {
			panic(err)
		}
	}
	subscription, err := client.SubscribeToTopic(config.Topic)
	if err != nil {
		log.Println(err)
	}
	var actions []string
	ticker := time.Tick(1 * time.Second)
	edit := time.Tick(15 * time.Minute)
	m := sync.Mutex{}
	for {
		select {
		case messages := <-subscription:
			new_actions := config.ApplyMoves(messages...)
			m.Lock()
			actions = append(actions, new_actions...)
			m.Unlock()
			log.Println(actions)
		case <-ticker:
			m.Lock()
			total := len(actions)
			m.Unlock()
			if total == 0 {
				continue
			}
			if err := client.PostInTopic(config.Topic, strings.Join(actions, "\n")); err != nil {
				log.Println(err)
			}
			if err := roulette.SaveGame("roulette.json", config); err != nil {
				log.Println(err)
			}
			m.Lock()
			actions = []string{}
			m.Unlock()
		case <-edit:
			var scoreboard []string
			for _, p := range config.Points {
				scoreboard = append(scoreboard, p.String())
			}
			client.EditPost(config.Topic, config.OP, OP+"\n"+strings.Join(scoreboard, "\n"))
		}
	}
}
