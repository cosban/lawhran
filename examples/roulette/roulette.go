package roulette

import (
	"math/rand"
	"time"
)

type Roulette struct {
	LoadedChamber  int `json:"loaded_chamber,omitempty"`
	CurrentChamber int `json:"current_chamber,omitempty"`
	seed           *rand.Rand
}

func NewGame() *Roulette {
	r := &Roulette{
		seed: rand.New(rand.NewSource(time.Now().Unix())),
	}
	r.Spin()
	return r
}

// returns whether to award points
func (r *Roulette) Shoot() bool {
	if r.LoadedChamber == r.CurrentChamber {
		r.Spin()
		return false
	}
	r.CurrentChamber++
	return true
}

// returns the number of chambers remaining
func (r *Roulette) Spin() int {
	remaining := r.LoadedChamber - r.CurrentChamber
	r.CurrentChamber = 0
	r.LoadedChamber = r.seed.Intn(6)
	return remaining
}
