package roulette

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	"gitlab.com/cosban/lawhran/eti"
)

type Configuration struct {
	Topic  uint              `json:"topic,omitempty"`
	OP     uint              `json:"op"`
	Points map[uint64]Player `json:"points"`
	State  *Roulette         `json:"state"`
	Banned map[uint64]bool   `json:"banned"`
}

type Player struct {
	ID     uint64 `json:"id"`
	Name   string `json:"name"`
	Points int    `json:"points"`
}

func (p Player) String() string {
	return fmt.Sprintf("%s: %d points", p.Name, p.Points)
}

func (c *Configuration) ApplyMoves(messages ...eti.Message) []string {
	var actions []string
	for _, message := range messages {
		if _, ok := c.Banned[message.PosterID]; ok {
			continue
		}
		player, ok := c.Points[message.PosterID]
		if !ok {
			player = Player{message.PosterID, message.Poster, 0}
		}
		lines := strings.Split(strings.ToLower(message.Message), "\n")
		for _, line := range lines {
			if strings.HasPrefix(line, "shoot") {
				if c.State.Shoot() {
					player.Points++
					actions = append(actions, fmt.Sprintf("%s pulls the trigger... click! They now have %d points.", player.Name, player.Points))
				} else {
					player.Points = 0
					actions = append(actions, fmt.Sprintf("%s pulls the trigger... BANG! They now have %d points.", player.Name, player.Points))
				}
				break
			} else if strings.HasPrefix(line, "spin") {
				remaining := c.State.Spin()
				if remaining == 0 {
					player.Points += 5
					actions = append(actions, fmt.Sprintf("%s spins the cylinder... Good thing too, they would have died. They now have %d points.", player.Name, player.Points))
				} else {
					if player.Points > -10 {
						player.Points -= 5
					}
					actions = append(actions, fmt.Sprintf("%s spins the cylinder... What a waste of empty chambers. They now have %d points.", player.Name, player.Points))
				}
				break
			}
		}
		c.Points[player.ID] = player
	}
	return actions
}

func LoadGame(name string) *Configuration {
	b, err := ioutil.ReadFile(name)
	if err != nil {
		panic(err)
	}
	var state Configuration
	err = json.Unmarshal(b, &state)
	if err != nil {
		panic(err)
	}
	if state.Points == nil {
		state.Points = map[uint64]Player{}
	}
	if state.State.seed == nil {
		state.State = NewGame()
	}
	return &state
}

func SaveGame(name string, state *Configuration) error {
	b, err := json.Marshal(state)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(name, b, 777)
}
