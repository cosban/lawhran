package main

import (
	"fmt"
	"log"

	"gitlab.com/cosban/lawhran/eti"
)

const (
	FirstThread  = 2162401
	SecondThread = 2162402
)

var posterMap = map[uint64]uint{
	3048:  FirstThread,
	24076: SecondThread,
}

func main() {
	client := eti.Client()

	firstSubscription, err := client.SubscribeToThread(FirstThread)
	if err != nil {
		panic(err)
	}
	secondSubscription, err := client.SubscribeToThread(SecondThread)
	if err != nil {
		panic(err)
	}
	onMessage := func(messages []eti.Message) {
		for _, message := range messages {
			log.Println(message.Message)
			replyThread, ok := posterMap[message.PosterID]
			if !ok {
				continue
			}
			if err := client.PostInThread(replyThread, fmt.Sprintf("Message from %s:\n<quote>%s</quote>", message.Poster, message.Message)); err != nil {
				log.Println(err)
			}
		}
	}

	for {
		select {
		case messages := <-firstSubscription:
			onMessage(messages)
		case messages := <-secondSubscription:
			onMessage(messages)
		}
	}
}
