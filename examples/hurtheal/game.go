package hurtheal

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"sort"
	"strings"
	"time"

	"gitlab.com/cosban/lawhran/eti"
)

type Stats struct {
	HP            int            `json:"hp,omitempty"`
	HealsGiven    map[uint64]int `json:"heals_given"`
	HealsReceived int            `json:"heals_received,omitempty"`
	HurtsGiven    map[uint64]int `json:"hurts_given"`
	HurtsReceived int            `json:"hurts_received,omitempty"`
	KillAssists   int            `json:"kill_assists,omitempty"`
	Kills         int            `json:"kills,omitempty"`
	Moves         int            `json:"moves,omitempty"`
	Dead          bool           `json:"dead,omitempty"`
	Killer        string         `json:"killer,omitempty"`
	Death         time.Time      `json:"death,omitempty"`
	Murdered      []string       `json:"murdered,omitempty"`
}

func (s Stats) String(state GameState) string {
	b := strings.Builder{}
	nl := "\n"
	b.WriteString(fmt.Sprintf("  Moves: %d", s.Moves))
	b.WriteString(nl)
	b.WriteString("  Heals Given: ")
	for id, amount := range s.HealsGiven {
		recipient := state.Players[id]
		b.WriteString(nl)
		b.WriteString(fmt.Sprintf("    * %s: %d", recipient.Name, amount))
	}
	b.WriteString("  Hurts Given: ")
	for id, amount := range s.HurtsGiven {
		recipient := state.Players[id]
		b.WriteString(nl)
		b.WriteString(fmt.Sprintf("    * %s: %d", recipient.Name, amount))
	}
	b.WriteString(nl)
	b.WriteString(fmt.Sprintf("  Heals Recieved: %d", s.HealsReceived))
	b.WriteString(nl)
	b.WriteString(fmt.Sprintf("  Hurts Recieved: %d", s.HurtsReceived))
	b.WriteString(nl)
	b.WriteString(fmt.Sprintf("  Murders: %s", strings.Join(s.Murdered, ", ")))
	return b.String()
}

type Player struct {
	ID    uint64    `json:"id"`
	Name  string    `json:"name"`
	Last  time.Time `json:"last"`
	Stats *Stats    `json:"stats"`
}

type GameState struct {
	LastMessage time.Time         `json:"last_message"`
	Topic       uint              `json:"topic"`
	Players     map[uint64]Player `json:"yetis"`
	Dead        map[uint64]bool   `json:"-"`
	New         []string          `json:"-"`
}

func FromFile(name string) *GameState {
	b, err := ioutil.ReadFile(name)
	if err != nil {
		panic(err)
	}
	var state GameState
	err = json.Unmarshal(b, &state)
	if err != nil {
		panic(err)
	}
	return NewGame(state.Topic, state.Players)
}

func NewGame(topic uint, players map[uint64]Player) *GameState {
	dead := map[uint64]bool{}
	for id, p := range players {
		if p.Stats == nil {
			p.Stats = (&GameState{}).newStats()
			players[id] = p
		} else if p.Stats.HP == 0 {
			dead[p.ID] = true
		}
	}
	return &GameState{
		Topic:   topic,
		Players: players,
		Dead:    dead,
	}
}

func (s *GameState) OP() string {
	return fmt.Sprintf(`<b>THE RULES</b>
<quote>- one post per round. i will post roughly <b>EVERY 10 MINUTES</b> until the game is complete.
    - Only one account per user may enter. <b>NO ALT ACCOUNTS</b>.
	- Two moves per post. i.e. "double hurt", "hurt/hurt", "hurt/heal", "heal/hurt" OR a single "heal"
	- health is set to a maximum of 20hp and double heals are banned
	- a contestant is dead the moment their hp hits 0. heals after their hp hits 0 are ignored
    - special rules may apply...
	- <b>edited posts will be skipped</b>
</quote>

If I find out you are breaking any rules, I will ban you from interacting with the bot permanently.

<b>For two moves, make sure you put them on separate lines. I do not care about casing. Feel free to shitpost in any order as long as your moves are on separate lines from everything else. If you try to cheat, your posts will be ignored.</b>
<b>The correct formats for each move type are as follows: (NOTE: if you do not use the correct format, your move will be skipped.)</b>
	- <pre>hurt {contestant}</pre>
	- <pre>heal {contestant}</pre>
	- <pre>double hurt {contestant}</pre>

TIP: if you are going to quote someone, do it after your move. Quoting someone else's move may cause you do copy their move.

i am literally a robot, so if you do not follow the format, that is on you.

Where's the chaos though? You'll see...

The following users are currently in the ring of death:
%s`, strings.Join(s.Participants(), "\n"))
}

func (s *GameState) Update() string {
	return fmt.Sprintf(`ROUND OVER! here is the state of the game:

%s`, s.CurrentState())
}

func (s *GameState) Victory() string {
	return fmt.Sprintf(`WE HAVE A WINNER! THE YETIEST YETI IS:

%s

Oh... Here are some stats I guess:

%s`, s.CurrentState(), s.PlayerStats())
}

func (s *GameState) PlayerStats() string {
	b := strings.Builder{}
	for _, p := range s.Players {
		b.WriteString(p.Name)
		b.WriteString(p.Stats.String(*s))
		b.WriteString("\n")
	}
	return b.String()
}

func (s *GameState) Participants() []string {
	var p []string
	for _, k := range s.Players {
		p = append(p, k.Name)
	}
	sort.Slice(p, func(i, j int) bool {
		return strings.ToLower(p[i]) < strings.ToLower(p[j])
	})
	return p
}

func (s *GameState) CurrentState() string {
	var dead []Player
	var alive []Player
	for _, p := range s.Players {
		if p.Stats.Dead {
			dead = append(dead, p)
		} else {
			alive = append(alive, p)
		}
	}

	buf := strings.Builder{}
	if len(s.New) > 0 {
		buf.WriteString("<b>New Contenders</b>\n")
		buf.WriteString(strings.Join(s.New, ", "))
		buf.WriteString("\n")
		s.New = []string{}
	}
	buf.WriteString("<b>Contenders</b>\n")
	sort.Slice(alive, func(i, j int) bool {
		return strings.ToLower(alive[i].Name) < strings.ToLower(alive[j].Name)
	})
	for i, p := range alive {
		buf.WriteString(fmt.Sprintf("%d: %s HP: %d\n", i+1, p.Name, p.Stats.HP))
	}
	if len(dead) > 0 {
		buf.WriteString("\n<b>DEAD</b>\n")
	}
	sort.Slice(dead, func(i, j int) bool {
		return dead[i].Stats.Death.Before(dead[j].Stats.Death)
	})
	for _, p := range dead {
		buf.WriteString(fmt.Sprintf("%s was killed by: %s at %v\n", p.Name, p.Stats.Killer, p.Stats.Death.Format("2006-01-02 15:04:05")))
	}
	return buf.String()
}

func (s *GameState) Apply(now time.Time, messages []eti.Message) bool {
	for _, message := range messages {
		if message.Poster == "lawhran" && message.Time.After(s.LastMessage) {
			s.LastMessage = message.Time
			continue
		}
		player, ok := s.Players[message.PosterID]
		if !ok && message.Poster != "lawhran" {
			player = Player{
				ID:    message.PosterID,
				Name:  message.Poster,
				Stats: s.newStats(),
			}
			s.Players[player.ID] = player
			s.New = append(s.New, message.Poster)
		}
		if message.Time.After(s.LastMessage) {
			if !ok || player.Last.Before(s.LastMessage) {
				s.applyMoves(strings.ToLower(message.Message), player, message.Time)
				if len(s.Players)-1 == len(s.Dead) {
					return true
				}
			}
		}
	}
	s.LastMessage = now
	return len(s.Dead) >= len(s.Players)-1
}

func (s *GameState) newStats() *Stats {
	maxHP := 0
	for _, p := range s.Players {
		if p.Stats.HP > maxHP {
			maxHP = p.Stats.HP
		}
	}
	startingHP := 10
	if maxHP < startingHP && maxHP != 0 {
		startingHP = int(math.Ceil(float64(maxHP) / 2.0))
	}
	return &Stats{
		HP:         startingHP,
		HurtsGiven: map[uint64]int{},
		HealsGiven: map[uint64]int{},
	}
}

func (s *GameState) applyMoves(message string, poster Player, posted time.Time) {
	lines := strings.Split(strings.ToLower(message), "\n")
	moves := 0
	healed := false
	for _, line := range lines {
		if moves >= 2 {
			break
		}
		if strings.HasPrefix(line, "double hurt ") {
			if moves == 0 {
				name := strings.TrimPrefix(line, "double hurt ")
				s.Hurt(name, poster, posted)
				s.Hurt(name, poster, posted)
				moves = 2
			} else {
				break
			}
		} else if strings.HasPrefix(line, "hurt ") {
			name := strings.TrimSpace(strings.TrimPrefix(line, "hurt "))
			s.Hurt(name, poster, posted)
			moves++
		} else if strings.HasPrefix(line, "heal ") {
			if healed {
				break
			}
			name := strings.TrimSpace(strings.TrimPrefix(line, "heal "))
			s.Heal(name, poster)
			healed = true
			moves++
		}
	}
	if moves > 0 {
		poster.Stats.Moves += moves
		poster.Last = posted
		s.Players[poster.ID] = poster
	}
}

func (s *GameState) fromName(name string) (Player, bool) {
	for _, p := range s.Players {
		if strings.ToLower(p.Name) == strings.ToLower(name) {
			return p, true
		}
	}
	return Player{}, false
}

func (s *GameState) Hurt(name string, poster Player, posted time.Time) {
	log.Printf("hurt detected by '%s' for '%s'", poster.Name, name)
	victim, ok := s.fromName(name)
	if ok && !victim.Stats.Dead {
		victim.Stats.HP--
		poster.Stats.HurtsGiven[victim.ID]++
		victim.Stats.HurtsReceived++
		if victim.Stats.HP == 0 {
			s.kill(victim, poster, posted)
		}
		s.Players[victim.ID] = victim
	}
}

func (s *GameState) kill(victim, murderer Player, at time.Time) {
	victim.Stats.Killer = murderer.Name
	murderer.Stats.Murdered = append(murderer.Stats.Murdered, victim.Name)
	victim.Stats.Dead = true
	victim.Stats.Death = at
	s.Dead[victim.ID] = true
}

func (s *GameState) Heal(name string, poster Player) {
	log.Printf("heal detected for '%s'", name)
	state, ok := s.fromName(name)
	if ok && !state.Stats.Dead && state.Stats.HP < 20 {
		state.Stats.HP++
		state.Stats.HealsReceived++
		poster.Stats.HealsGiven[state.ID]++
		s.Players[state.ID] = state
	}
}
