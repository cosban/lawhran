package main

import (
	"context"
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"time"

	"gitlab.com/cosban/lawhran/examples/hurtheal"

	"golang.org/x/time/rate"

	"gitlab.com/cosban/lawhran/eti"
)

var (
	participantsFile string
	title            string
	dryRun           bool
)

func saveGame(name string, state *hurtheal.GameState) error {
	b, err := json.Marshal(state)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(name, b, 777)
}

func init() {
	flag.StringVar(&participantsFile, "participants", "yetis.json", "the file path to a list of names to hurt/heal")
	flag.StringVar(&title, "title", "Hurt/Heal im a robot edition", "the title of the new topic")
	flag.BoolVar(&dryRun, "dry-run", false, "if true, the contents of the topic will be posted to stderr instead of to eti")
	flag.Parse()
}

func etiClient(dry bool) *eti.ETIClient {
	if dry {
		return eti.DryClient()
	}
	return eti.Client()
}

func postNewTopic(client *eti.ETIClient, state *hurtheal.GameState, title string) uint {
	topic, err := client.CreateNewTopic(title, state.OP())
	if err != nil {
		log.Fatal(err)
	}
	return topic
}

func main() {
	state := hurtheal.FromFile(participantsFile)
	client := etiClient(dryRun)

	limiter := rate.NewLimiter(rate.Every(10*time.Minute), 1)

	if state.Topic == 0 {
		if err := limiter.Wait(context.Background()); err != nil {
			panic(err)
		}
		state.Topic = postNewTopic(client, state, title)
		if err := saveGame(participantsFile, state); err != nil {
			panic(err)
		}
	}

	for {
		if err := limiter.Wait(context.Background()); err != nil {
			log.Println(err)
			continue
		}
		log.Println("running points check")
		now := time.Now()
		t, err := client.ReadTopicMessages(state.Topic)
		if err != nil {
			panic(err)
		}
		finished := state.Apply(now, t.Messages)
		if finished {
			break
		}
		err = client.PostInTopic(state.Topic, state.Update())
		if err != nil {
			log.Println(err)
		}
		if err := saveGame(participantsFile, state); err != nil {
			panic(err)
		}
	}
	if err := client.PostInTopic(state.Topic, state.Victory()); err != nil {
		log.Println(err)
	}
}
